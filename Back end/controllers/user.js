const bcrypt = require('bcrypt');
const { OAuth2Client } = require('google-auth-library');

const User = require('../models/User');
const Category = require('../models/Category');
const auth = require('../auth');

const clientId = '680870623230-1cc87qmmgv46udqo0np63amb0oa3o8hs.apps.googleusercontent.com'
const errCatcher = err => console.log(err)

module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10),
		loginType: 'email'
	})

	return user.save().then((user, err) => {
		return (err) ? false : true
	})
}

module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then(user => {
		if (user === null) {
			return { error: 'does-not-exist' };
		}
		if (user.loginType !== 'email') {
			return { error: 'login-type-error'};
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if (isPasswordMatched) {
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return { error: 'incorrect-password' };
		}
	})
}

module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}


// module.exports.updateDetails = (params) => {
	
// }

module.exports.editPassword = (params) => {

	const user = User.findById(params.userId)
	console.log(user)

	// if(bcrypt.compareSync(params.password, user.password)) {
		const updates = {
			firstName: params.firstName,
			lastName: params.lastName,
			mobileNo: params.mobileNo,
			email: params.email,
			password: bcrypt.hashSync(params.password, 10)
		}

		return User.findByIdAndUpdate(params.userId, updates).then((doc, err) => {
			return (err) ? false : true
		})
}

module.exports.verifyGoogleTokenId = async (params) => {
	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({
		idToken: params,
		audience: clientId
	})

	if(data.payload.email_verified === true){
		const user = await User.findOne({ email: data.payload.email }).exec();

		if (user !== null) {
			if (user.loginType === "google") {
				return {
					accessToken: auth.createAccessToken(user.toObject())
				}
			} else {
				return { error: "login-type-error" }
			}
		} else {

			const newUser = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: "google"
			})

			return newUser.save().then((user, err) => {
				return {
					accessToken: auth.createAccessToken(user.toObject())
				}
			})
		}
	} else {
		return { error: "google-auth-error" }
	}
}

// module.exports.enroll = (params) => {
// 	return User.findById(params.userId).then(user => {
// 		records.push({ amount: params.amount })

// 		return user.save().then((category, err) => {
// 			return Category.findById(params.categoryId).then(category => {
				
// 				category.enrollees.push({ userId: params.userId })

// 				return category.save().then((category, err) =>{
// 					if (err) {
//                         return false
//                     } else {
//                         return true
//                     }
// 				})
// 			})
// 		})
// 	})
// }

// module.exports.enroll = (params) => {
//     return User.findById(params.userId)
//     .then((user, err) => {
//         if(err) return false
//         user.records.push(params.records)
//         return user.save()
//         .then((user, err) => {
//         	return Category.findById(params.categoryId).then(category => {
//         		category.enrollees.push({ userId: params.userId })
//         	return category.save()
//         	})
//         })
//         .catch(errCatcher)
//     })
//     .catch(errCatcher)
// }

// module.exports.enroll = (params) => {
// 	return User.findById(params.userId).then(user => {
// 		user.records.push(params.records)

// 		return user.save().then((user, err) => {
// 			return Category.findById(params.categoryId).then(category => {
// 				category.enrollees.push({ userId: params.userId })

// 				return category.save().then((category, err) =>{
// 					if (err) {
//                         return false
//                     } else {
//                         return true
//                     }
// 				})
// 			})
// 		})
// 	})
// }