const Category = require('../models/Category');

module.exports.addCategory = (params) => {
	let category = new Category({
		name: params.name,
		description: params.description,
		type: params.type,
		amount: params.amount
	})

	return category.save().then((category, err) => {
		return (err) ? false : true
	})
}

module.exports.getAll = () => {
	return Category.find().then(categories => categories)
}

module.exports.get = (params) => {
	return Category.findById(params.categoryId).then(category => category)
}

module.exports.update = (params) => {

	const updates = {
		name: params.name,
		description: params.description,
		type: params.type,
		amount: params.amount
	}

	return Category.findByIdAndUpdate(params.categoryId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}

module.exports.delete = (params) => {

	const updates = {
		name: params.name,
		description: params.description,
		type: params.type,
		amount: params.amount
		}

	return Category.findByIdAndDelete(params.categoryId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}
