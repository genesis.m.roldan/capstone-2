const mongoose = require('mongoose')

const categorySchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'category name is required']
	},
	description: {
		type: String,
		required: [true, 'category description is required']
	},
	type: {
		type: String,
		required: [true, 'Type is required']
	},
	amount: {
		type: Number,
		required: [true, 'Amount is required']
	},
	enrolledOn: {
				type: Date,
				default: new Date()
			}		
})

module.exports = mongoose.model('Category', categorySchema);