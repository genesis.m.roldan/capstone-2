const express = require('express');
const router = express.Router();
const UserController = require('../controllers/user')
const auth = require('../auth');

// 1. create a route
// 2. create controller logic

router.post('/email-exists', (req, res) => {
	UserController.emailExists(req.body).then(result =>{
		res.send(result)
	})
})

router.post('/', (req, res) => {
	UserController.register(req.body).then(result =>{
		res.send(result)
	})
})

router.post('/login', (req, res) => {
	UserController.login(req.body).then(result =>{
		res.send(result)
	})
})

router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization);
	UserController.get({ userId: user.id }).then(user => res.send(user))
})

router.put('/details', (req, res) => {
	const user = auth.decode(req.headers.authorization);
	const params = {
		userId: user.id,
		email: req.body.email,
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo,
		password: req.body.password
	}
    UserController.editPassword(params)
    .then(data => {
    	res.send(data)
    })
})

router.post('/records', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		records: {
			categoryId: req.body.categoryId,
			amount: req.body.amount
		}
	}
	UserController.enroll(params).then(result => res.send(result))
})


router.post('/verify-google-id-token', async (req, res) => {
    res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

module.exports = router;