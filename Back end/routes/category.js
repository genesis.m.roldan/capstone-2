const express = require('express');
const router = express.Router();
const auth = require('../auth');
const CategoryController = require('../controllers/category');

router.post('/', auth.verify, (req, res) => {
	CategoryController.addCategory(req.body).then(result => res.send(result))
})

router.get('/', (req, res) => {
	CategoryController.getAll().then(categories => res.send(categories))	
})

router.get('/:categoryId', (req, res) => {
	const categoryId = req.params.categoryId
	CategoryController.get({ categoryId }).then(category => res.send(category))
})

router.put('/', auth.verify, (req, res) => {
	CategoryController.update(req.body).then(result => res.send(result))
})

router.delete('/:categoryId', auth.verify, (req, res) => {
	const categoryId = req.params.categoryId
	CategoryController.delete({ categoryId }).then(result => res.send(result))
})


module.exports = router