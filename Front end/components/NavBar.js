import { useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';
import Link from 'next/link';

export default function NavBar() {
	const { user } = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg">
            <Link href="/">
                <a className="navbar-brand">Budget Tracker</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                	<Link href="/">
                        <a className="nav-link" role="button">Home</a>
                    </Link>
                    {(user.email !== null)
                        ?
                        <React.Fragment>
                            <Link href="/logout">
                                <a className="nav-link" role="button">Logout</a>
                            </Link>
                            <Link href="/editPassword">
                                <a className="nav-link" role="button">Edit Account</a>
                            </Link>
                             <Link href="/category/editCategory">
                                <a className="nav-link" role="button">Categories</a>
                            </Link>

                            <Link href="/record">
                                <a className="nav-link" role="button">Records</a>
                            </Link>

                        </React.Fragment> 
                        : 
                        <React.Fragment>
                            <Link href="/login">
                                <a className="nav-link" role="button">Login</a>
                            </Link>
                            <Link href="/register">
                                <a className="nav-link" role="button">Register</a>
                            </Link>
                        </React.Fragment>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
	)
}