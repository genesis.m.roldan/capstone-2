import { useState } from 'react';
import { Card, Form, Button } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function Category() {
    
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [type, setType] = useState('Income');
    const [amount, setAmount] = useState('');

    
    function addCategory(e) {
        e.preventDefault();

        fetch('http://localhost:4000/api/categories', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                type: type,
                amount: amount
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            if (data === true){
                Swal.fire({
                    title: "Category added!",
                    icon: "success",
                })
                Router.push('/category/editCategory')
            } else {
                Router.push('/errors/1')
            }
        })
    }

    return (
    <React.Fragment>
        <Form onSubmit={(e) => addCategory(e)} className="col-lg-4 offset-lg-4 my-5">
            <Form.Group controlId="name">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control 
                    type="text"
                    placeholder="Enter category name:"
                    value={name}
                    onChange={e => setName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="description">
                <Form.Label>Category Description:</Form.Label>
                <Form.Control
                    as="textarea"
                    rows="3"
                    placeholder="Enter category description:"
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="type">
                <Form.Label>Category Type:</Form.Label>
                    <select className="custom-select" 
                        value={type}
                        onChange={e => setType(e.target.value)}
                        required>
                        <option> Income </option>
                        <option> Expense </option>
                    </select>
            </Form.Group>

            <Form.Group controlId="amount">
                <Form.Label>Amount:</Form.Label>
                <Form.Control 
                    type="number"
                    placeholder="Enter amount:"
                    value={amount}
                    onChange={e => setAmount(e.target.value)}
                    required
                />
            </Form.Group>

            <Button className="bg-primary" type="submit">
                Submit
            </Button>
        </Form>

    </React.Fragment>
    )
}

