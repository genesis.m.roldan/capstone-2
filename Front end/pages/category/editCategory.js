import { useContext, useState, useEffect } from 'react';
import { Table, Button, Modal, Form, Card } from 'react-bootstrap';
import UserContext from '../../UserContext';
import Head from 'next/head';
import Router from 'next/router';
import Swal from 'sweetalert2';
import Category from '../category/index';


export default function index({ data }) {
	const { user } = useContext(UserContext);

	// states for edit form input
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [amount, setAmount] = useState('');
	const [type, setType] = useState('');

	// state for course ID to be edited
	const [categoryId, setCategoryId] = useState('');

	// state for showing and closing the modal edit form
	const [showForm, setShowForm] = useState(false);
	const [showForm2, setShowForm2] = useState(false);

	// state for storing JWT
	const [token, setToken] = useState('');
	

	// sets Token from localstorage
	useEffect(() => {
		setToken(localStorage.getItem('token'))
	})

	const handleClose = () => setShowForm(false);
	const handleClose2 = () => setShowForm2(false);

	const handleShow = (categoryId) => {

		fetch(`http://localhost:4000/api/categories/${categoryId}`)
		.then(res => {
			return res.json()
		})
		.then(data => {
			console.log(data)
			setCategoryId(data._id)
			setName(data.name)
			setDescription(data.description)
			setType(data.type)
			setShowForm(true);
		})
	}

	function editCategory(e) {
		e.preventDefault();

		fetch('http://localhost:4000/api/categories', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				categoryId: categoryId,
				name: name,
				description: description,
				type: type,
				amount: amount
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {

			// if update course successful
			if(data === true){
				Swal.fire({
                    title: "Category updated.",
                    icon: "success",
                })
				Router.push('/category/editCategory')
				setShowForm(false)
			}else{
				// error in editing page
				Router.push('/errors/1')
				setShowForm(false)
			}

		})
	}

	function deleteCategory(categoryId) {
		fetch(`http://localhost:4000/api/categories/${categoryId}`, {
			method: 'DELETE',
			headers: {
				'Authorization': `Bearer ${token}`
			}
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			// if archive course succesful
			if(data === true){
				Swal.fire({
                    title: "Category deleted.",
                    icon: "success",
                })
				Router.push('/category/editCategory')
			} else {
				// error in archiving course
				Router.push('/errors/1')
			}
		})
	}

	function enroll(e){
        fetch('http://localhost:4000/api/users/records', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
            	categoryId: categoryId,
            	amount: amount
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
        	conole.log(data)
            if(data === true){
            	console.log(data)
				Swal.fire({
                    title: "Record added.",
                    icon: "success",
                })
				Router.push('/')
			} else {
				// error in archiving course
				console.log(err)
			}
        })
    }

	const categoryRows = data.map(indivCategory => {
		return (
			<tr key={indivCategory._id}>
				<td>{indivCategory.name}</td>
				<td>{indivCategory.description}</td>
				<td>{indivCategory.type}</td>
				<td>{indivCategory.amount}</td>
				<td>
					<Button className="bg-warning" onClick={() => {handleShow(indivCategory._id)}}>Update</Button>
					<Button className="bg-danger" onClick={() => {deleteCategory(indivCategory._id)}}>Delete</Button>
				</td>
			</tr>
		)
	})

	return (
		
		<React.Fragment>
		<Category /> 
			<Head>
				<title>Dashboard</title>
			</Head>
			<h1>Transactions Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Type</th>
						<th>Amount</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ categoryRows }
				</tbody>
			</Table>

			<Modal show={showForm} onHide={handleClose}>
			    <Modal.Header closeButton>
			        <Modal.Title>Update {name}</Modal.Title>
			    </Modal.Header>
			    <Modal.Body>
			        <Form onSubmit={(e) => editCategory(e)}>

			            <Form.Group controlId="name">
			                <Form.Label>Category Name:</Form.Label>
			                <Form.Control 
			                    type="text" 
			                    placeholder="Enter category name" 
			                    value={name} 
			                    onChange={e => {setName(e.target.value)}} 
			                    required
			                />
			            </Form.Group>

			            <Form.Group controlId="description">
			                <Form.Label>Category Description:</Form.Label>
			                <Form.Control 
			                    as="textarea" 
			                    rows="3" 
			                    placeholder="Enter category description" 
			                    value={description} 
			                    onChange={e => {setDescription(e.target.value)}} 
			                    required
			                />
			            </Form.Group>

			            <Form.Group controlId="type">
			                <Form.Label>Category Type:</Form.Label>
			                <select className="custom-select" 
                        		value={type}
                        		onChange={e => setType(e.target.value)}
                        		required>
                        		<option> Income </option>
                        		<option> Expense </option>
                    		</select>
			            </Form.Group>

			            <Form.Group controlId="amount">
			                <Form.Label>Amount:</Form.Label>
            				    <Form.Control 
				                    type="number"
				                    placeholder="Enter amount"
				                    value={amount}
				                    onChange={e => setAmount(e.target.value)}
				                    required
                					/>
            			</Form.Group>

			            <Button className="bg-primary" type="submit">Submit</Button>
			        </Form>
			    </Modal.Body>
			    <Modal.Footer>
			        <Button className="bg-secondary" onClick={handleClose}>
			            Close
			        </Button>
			    </Modal.Footer>
			</Modal>
		</React.Fragment>
	)
}

export async function getServerSideProps() {
	// fetch data from endpoint
	const res = await fetch('http://localhost:4000/api/categories')
	const data = await res.json()

	//return the props
	return {
		props: {
			data
		}
	}
}
