import { useState, useEffect } from 'react';
import { Table, Alert, Row, Col } from 'react-bootstrap';
import Head from 'next/head'

export default function Record() {

	const [records, setRecords] = useState([])
	const [income, setIncome] = useState('')
	const [expense, setExpense] = useState('')
	const [transactions, setTransactions] = useState([])
	const [total, setTotal] = useState([])
	const [diff, setDiff] = useState([])


	useEffect(() => {
		fetch('http://localhost:4000/api/categories', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				setRecords(data)
			} else {
				setRecords([])
			}
		})
	}, [])

	useEffect(() => {
		fetch('http://localhost:4000/api/categories', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				setTransactions(data)
				transactions.map(transaction => {
					if(transaction.type === "Income") {
						total.push(transaction.amount)
						setTotal(total)
					} else if(transaction.type === "Expense"){
						diff.push(transaction.amount)
						setDiff(diff)
					}
				})
			} else {
				setTransactions([])
			}
		})
	}, [setTransactions])


    return (
    	<React.Fragment>
    		<Head>
    			<title> Transaction Records </title>
    		</Head>
    		<Row>
				<Col xs={12} lg={6}>
					{records.length > 0 
					?
					<Table striped bordered hover>
						<thead>
							<tr>
								<th>Name</th>
								<th>Description</th>
								<th>Type</th>
								<th>Amount (Php)</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							{records.map(record => {
								return(
									<tr key={record._id}>
										<td> {record.name} </td>
										<td> {record.description} </td>
										<td> {record.type} </td>
										<td> {record.amount} </td>
										<td> {record.enrolledOn} </td>
									</tr>
								)
							})}
						</tbody>
					</Table>
					:
					<Alert align="center" variant="info"> You have no transaction records yet. </Alert>
					}
				</Col>
			</Row>
		<h1> {total} </h1>
		</React.Fragment>
    )
}
