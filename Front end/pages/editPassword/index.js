import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function index() {
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [user, setUser] = useState('');
	const [isActive, setIsActive] = useState(false);


	function editPassword(e) {
		e.preventDefault();

		fetch('http://localhost:4000/api/users/details', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
                    title: "Account updated.",
                    icon: "success",
                })
				Router.push('/')
			} else {
				Router.push('/error')
			}
		})
	}

	useEffect(() => {
		if((password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else{
		setIsActive(false);
		}
	},[password1, password2]);


	return (
		<Form onSubmit={e => editPassword(e)} className="col-lg-4 offset-lg-4 my-5">
			
			<Form.Group>
				<Form.Label>First Name </Form.Label>
				<Form.Control 
					type="text"
					placeholder="Input First Name"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Input last name"
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>


			{isActive
				?
				<Button 
					className="bg-primary"
					type="submit"
					id="submitBtn"
				>
					Submit
				</Button>
				:
				<Button 
					className="bg-danger"
					type="submit"
					id="submitBtn"
					disabled
				>
					Submit
				</Button>
			}
			
			
		</Form>
	)
};

