// this helper file is used to shorten code in various parts of the project

module.exports = {
	API_URL: process.env.NEXT_PUBLIC_API_URL,
	getAccessToken: () => localStorage.getItem('token'),
	toJSON: (response) => response.json()
}